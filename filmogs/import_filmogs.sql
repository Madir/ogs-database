-- WARNING script completely removes the previous database

\echo
\echo STARTED SCRIPT FILMOGS-IMPORT
\echo =============================
\echo

\pset pager off

\echo
\echo Remove old database and user
DROP DATABASE filmogs;
DROP OWNED BY filmogs CASCADE;
DROP USER filmogs;

\echo
\echo Create database filmogs and user filmogs
CREATE DATABASE filmogs;
CREATE USER filmogs WITH PASSWORD 'filmogs';
GRANT ALL PRIVILEGES ON DATABASE filmogs TO filmogs;

\connect filmogs

\i ./import_filmogs_credits.sql
\i ./import_filmogs_companies.sql
\i ./import_filmogs_films.sql
\i ./import_filmogs_releases.sql

\pset pager on

\echo
\echo FINISHED SCRIPT FILMOGS-IMPORT
