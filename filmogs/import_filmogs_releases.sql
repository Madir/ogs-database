\echo 
\echo Start import releases
\echo ---------------------

-- Disabled because database is dropped
--DROP TABLE public.release_companies;
--DROP TABLE public.release_films;
--DROP TABLE public.release_links;
--DROP TABLE public.release_chapters;
--DROP TABLE public.release_classifications;
--DROP TABLE public.release_audio;
--DROP TABLE public.release_territories;
--DROP TABLE public.release_regions;
--DROP TABLE public.release_codes;
--DROP TABLE public.release_packagings;
--DROP TABLE public.release_formats;
--DROP TABLE public.releases;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE releases_import_step1;
DROP TABLE releases_import_step2;

\echo
\echo Create tables
CREATE TABLE public.releases (
    release_id integer NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    aspect_ratio text COLLATE pg_catalog."default",
    color_encoding_system text COLLATE pg_catalog."default",
    languages text COLLATE pg_catalog."default",
    subtitles text COLLATE pg_catalog."default",
    run_time text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    CONSTRAINT releases_pkey PRIMARY KEY (release_id)
) TABLESPACE pg_default;

CREATE TABLE public.release_formats (
    release_format_id serial NOT NULL,
    quantity text,
    type text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    release_id integer NOT NULL,
    CONSTRAINT release_formats_pkey PRIMARY KEY (release_format_id),
    CONSTRAINT release_formats_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_packagings (
    release_packaging_id serial NOT NULL,
    type text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    release_id integer NOT NULL,
    CONSTRAINT release_packagings_pkey PRIMARY KEY (release_packaging_id),
    CONSTRAINT release_packagings_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_codes (
    release_code_id serial NOT NULL,
    code text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    release_id integer NOT NULL,
    CONSTRAINT release_codes_pkey PRIMARY KEY (release_code_id),
    CONSTRAINT release_codes_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_regions (
    release_region_id serial NOT NULL,
    release_id integer NOT NULL,
    region text COLLATE pg_catalog."default",
    CONSTRAINT release_regions_pkey PRIMARY KEY (release_region_id),
    CONSTRAINT release_regions_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_territories (
    release_territory_id serial NOT NULL,
    release_id integer NOT NULL,
    territory text COLLATE pg_catalog."default",
    CONSTRAINT release_territories_pkey PRIMARY KEY (release_territory_id),
    CONSTRAINT release_territories_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_audio (
    release_audio_id serial NOT NULL,
    release_id integer NOT NULL,
    audio text COLLATE pg_catalog."default",
    CONSTRAINT release_audio_pkey PRIMARY KEY (release_audio_id),
    CONSTRAINT release_audio_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_classifications (
    release_classification_id serial NOT NULL,
    release_id integer NOT NULL,
    type text COLLATE pg_catalog."default",
    rating text COLLATE pg_catalog."default",
    CONSTRAINT release_classifications_pkey PRIMARY KEY (release_classification_id),
    CONSTRAINT release_classifications_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_chapters (
    release_chapter_id serial NOT NULL,
    release_id integer NOT NULL,
    position text COLLATE pg_catalog."default",
    title text COLLATE pg_catalog."default",
    duration text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    CONSTRAINT release_chapters_pkey PRIMARY KEY (release_chapter_id),
    CONSTRAINT release_chapters_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_links (
    release_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    release_id integer NOT NULL,
    CONSTRAINT release_links_pkey PRIMARY KEY (release_link_id),
    CONSTRAINT release_links_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_films (
    release_film_id serial NOT NULL,
    release_id integer NOT NULL,
    film_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT release_films_pkey PRIMARY KEY (release_film_id),
    CONSTRAINT release_films_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT release_films_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.release_companies (
    release_company_id serial NOT NULL,
    release_id integer NOT NULL,
    company_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    date text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT release_companies_pkey PRIMARY KEY (release_company_id),
    CONSTRAINT release_companies_company_id_fkey FOREIGN KEY (company_id)
        REFERENCES public.companies (company_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT release_companies_release_id_fkey FOREIGN KEY (release_id)
        REFERENCES public.releases (release_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to filmogs
ALTER TABLE public.releases OWNER to filmogs;
ALTER TABLE public.release_formats OWNER to filmogs;
ALTER TABLE public.release_packagings OWNER to filmogs;
ALTER TABLE public.release_codes OWNER to filmogs;
ALTER TABLE public.release_regions OWNER to filmogs;
ALTER TABLE public.release_territories OWNER to filmogs;
ALTER TABLE public.release_audio OWNER to filmogs;
ALTER TABLE public.release_classifications OWNER to filmogs;
ALTER TABLE public.release_chapters OWNER to filmogs;
ALTER TABLE public.release_links OWNER to filmogs;
ALTER TABLE public.release_films OWNER to filmogs;
ALTER TABLE public.release_companies OWNER to filmogs;

\echo
\echo Extract data from filmogs-releases.json
CREATE TEMP TABLE releases_import_step1(releases json);

\copy releases_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./filmogs-releases.json'

CREATE TEMP TABLE releases_import_step2(
	release_id int,
    	slug text,
	title text,
        aspect_ratio text,
	color_encoding_system text,
	languages text,
    	subtitles text,
    	run_time text,
    	notes text,
	formats json,
	packagings json,
	codes json,
	regions json,
	territories json,
	audio json,
	classifications json,
	chapters json,
	links json,
	films json,
	companies json
);

-- Extract single rows
INSERT INTO releases_import_step2(release_id, slug, title, aspect_ratio, color_encoding_system, languages, subtitles, run_time, notes, formats, packagings, codes, regions, territories, audio, classifications, chapters, links, films, companies)
SELECT id, slug, title, aspect_ratio, color_encoding_system, languages, subtitles, run_time, notes, format, packaging, identifying_codes, region, territories, audio, classification, chapters, external_links, film, companies FROM releases_import_step1,
json_to_recordset(releases_import_step1.releases::json) AS t(id int, slug text, title text, aspect_ratio text, color_encoding_system text, languages text, subtitles text, run_time text, notes text, format json, packaging json, identifying_codes json, region json, territories json, audio json, classification json, chapters json, external_links json, film json, companies json);

DROP TABLE releases_import_step1;

\echo
\echo Import to table releases
INSERT INTO releases (release_id, slug, title, aspect_ratio, color_encoding_system, languages, subtitles, run_time, notes) 
SELECT release_id, slug, title, aspect_ratio, color_encoding_system, languages, subtitles, run_time, notes FROM releases_import_step2;

\echo
\echo Import to table release_formats
INSERT INTO release_formats (quantity, type, description, release_id)
SELECT quantity,type,description,release_id FROM releases_import_step2,
json_to_recordset(releases_import_step2.formats::json) AS t(quantity text, type text, description text);

\echo
\echo Import to table release_packagings
INSERT INTO release_packagings (type, description, release_id)
SELECT type,description,release_id FROM releases_import_step2,
json_to_recordset(releases_import_step2.packagings::json) AS t(type text, description text);

\echo
\echo Import to table release_codes
INSERT INTO release_codes(code, type, notes, release_id)
SELECT code,type,t.notes,release_id FROM releases_import_step2,
json_to_recordset(releases_import_step2.codes::json) AS t(code text, type text, notes text);

\echo
\echo Import to table release_regions
INSERT INTO release_regions(region, release_id)
SELECT region,release_id FROM releases_import_step2,
json_array_elements_text(releases_import_step2.regions::json) as region;

\echo
\echo Import to table release_territories
INSERT INTO release_territories(territory, release_id)
SELECT territory,release_id FROM releases_import_step2,
json_array_elements_text(releases_import_step2.territories::json) as territory;

\echo
\echo Import to table release_audio
INSERT INTO release_audio(audio, release_id)
SELECT audio_,release_id FROM releases_import_step2,
json_array_elements_text(releases_import_step2.audio::json) as audio_;

\echo
\echo Import to table release_classifications
INSERT INTO release_classifications(type, rating, release_id)
SELECT type,rating,release_id FROM releases_import_step2,
json_to_recordset(releases_import_step2.classifications::json) AS t(type text, rating text);

\echo
\echo Import to table release_chapters
INSERT INTO release_chapters(position, title, duration, type, release_id)
SELECT position,t.title,duration,type,release_id FROM releases_import_step2,
json_to_recordset(releases_import_step2.chapters::json) AS t(position text, title text, duration text, type text);

\echo
\echo Import to table release_links
INSERT INTO release_links(url, notes, release_id)
SELECT url,t.notes,release_id FROM releases_import_step2,
json_to_recordset(releases_import_step2.links::json) AS t(url text, notes text);

\echo 
\echo Search for broken film-relations (film_id is NULL or film_id not in table films)
SELECT releases_import_step2.title,release_id,id,t.title,name_variation FROM releases_import_step2,
json_to_recordset(releases_import_step2.films::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT film_id FROM films WHERE film_id=id));

\echo
\echo Import to table releases_films
INSERT INTO release_films (release_id, film_id, title, name_variation)
SELECT release_id,id,t.title,name_variation FROM releases_import_step2,
json_to_recordset(releases_import_step2.films::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT film_id FROM films));

\echo 
\echo Search for broken company-relations (company_id is NULL or company_id not in table companies)
SELECT releases_import_step2.title,release_id,id,t.title,role,date,name_variation FROM releases_import_step2,
json_to_recordset(releases_import_step2.companies::json) AS t(id int, title text, role text, date text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT company_id FROM companies WHERE company_id=id));

\echo
\echo Import to table release_companies
INSERT INTO release_companies (release_id, company_id, title, role, date, name_variation)
SELECT release_id,id,t.title,role,date,name_variation FROM releases_import_step2,
json_to_recordset(releases_import_step2.companies::json) AS t(id int, title text, role text, date text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT company_id FROM companies));-- WHERE company_id=id));

-- remove temporary table
\echo
\echo Clean up
DROP TABLE releases_import_step2;
\echo
\echo Finished import releases
\echo
