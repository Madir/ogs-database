\echo 
\echo Start import films
\echo ------------------

-- Disabled because database is dropped
--DROP TABLE public.film_credits;
--DROP TABLE public.film_links;
--DROP TABLE public.film_countries;
--DROP TABLE public.film_sub_genres;
--DROP TABLE public.film_genres;
--DROP TABLE public.film_alternative_titles;
--DROP TABLE public.films;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE films_import_step1;
DROP TABLE films_import_step2;

\echo
\echo Create tables
CREATE TABLE public.films (
    film_id serial NOT NULL,
    title text COLLATE pg_catalog."default",
    slug text COLLATE pg_catalog."default" NOT NULL,
    premiere text COLLATE pg_catalog."default",
    category text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    CONSTRAINT films_pkey PRIMARY KEY (film_id),
    CONSTRAINT films_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.film_alternative_titles (
    film_alternative_title_id serial NOT NULL,
    title text COLLATE pg_catalog."default",
    language text COLLATE pg_catalog."default",
    film_id integer NOT NULL,
    CONSTRAINT film_alternative_titles_pkey PRIMARY KEY (film_alternative_title_id),
    CONSTRAINT films_alternative_titles_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.film_genres (
    film_genre_id serial NOT NULL,
    film_id integer NOT NULL,
    genre text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT film_genres_pkey PRIMARY KEY (film_genre_id),
    --CONSTRAINT film_genres_film_id_genre_key UNIQUE (film_id, genre),
    CONSTRAINT film_genres_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.film_sub_genres (
    film_sub_genre_id serial NOT NULL,
    film_id integer NOT NULL,
    sub_genre text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT film_sub_genres_pkey PRIMARY KEY (film_sub_genre_id),
    --CONSTRAINT film_sub_genres_film_id_genre_key UNIQUE (film_id, sub_genre),
    CONSTRAINT film_sub_genres_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.film_countries (
    film_country_id serial NOT NULL,
    film_id integer NOT NULL,
    country text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT film_countries_pkey PRIMARY KEY (film_country_id),
    --CONSTRAINT film_countries_film_id_country_key UNIQUE (film_id, country),
    CONSTRAINT film_countries_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.film_links (
    film_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    film_id integer NOT NULL,
    CONSTRAINT film_links_pkey PRIMARY KEY (film_link_id),
    CONSTRAINT film_links_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.film_credits (
    film_credit_id serial NOT NULL,
    film_id integer NOT NULL,
    credit_id integer NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    role text COLLATE pg_catalog."default",
    act_as text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT film_credits_pkey PRIMARY KEY (film_credit_id),
    CONSTRAINT film_credits_film_id_fkey FOREIGN KEY (film_id)
        REFERENCES public.films (film_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT film_credits_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to filmogs
ALTER TABLE public.films OWNER to filmogs;
ALTER TABLE public.film_alternative_titles OWNER to filmogs;
ALTER TABLE public.film_genres OWNER to filmogs;
ALTER TABLE public.film_sub_genres OWNER to filmogs;
ALTER TABLE public.film_countries OWNER to filmogs;
ALTER TABLE public.film_links OWNER to filmogs;
ALTER TABLE public.film_credits OWNER to filmogs;

\echo
\echo Extract data from filmogs-films.json
CREATE TEMP TABLE films_import_step1(films json);

\copy films_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./filmogs-films.json'

CREATE TEMP TABLE films_import_step2 (
	film_id integer,
	title text,
	premiere text,
	slug text,
	category text,
	notes text,
	alternative_titles json,
	credits json,
	genres json,
	sub_genres json,
	countries json,
	links json
);

-- Extract single rows
INSERT INTO films_import_step2 (film_id, title, premiere, slug, category, notes, alternative_titles, credits, genres, sub_genres, countries, links)
SELECT id,title,premiere,slug,category,notes,alternative_titles,credits,genres,"sub-genres",countries,external_links FROM films_import_step1,
json_to_recordset(films_import_step1.films::json) AS t(id int, title text, premiere text, slug text, category text, notes text, alternative_titles json, credits json, genres json, "sub-genres" json, countries json, external_links json);

DROP TABLE films_import_step1;

\echo
\echo Import to table films
INSERT INTO films (film_id, title, slug, premiere, category, notes)
SELECT film_id, title, slug, premiere, category, notes FROM films_import_step2;


\echo
\echo Import to table film_alternative_titles
INSERT INTO film_alternative_titles (title, language, film_id)
SELECT t.title,language,film_id FROM films_import_step2,
json_to_recordset(films_import_step2.alternative_titles::json) AS t(title text, language text);

\echo
\echo Import to table film_genres
INSERT INTO film_genres (film_id, genre) 
SELECT film_id,genre FROM films_import_step2,
json_array_elements_text(films_import_step2.genres::json) as genre;

\echo
\echo Import to table film_sub_genres
INSERT INTO film_sub_genres (film_id, sub_genre) 
SELECT film_id,sub_genre FROM films_import_step2,
json_array_elements_text(films_import_step2.sub_genres::json) as sub_genre;

\echo
\echo Import to table film_countries
INSERT INTO film_countries (film_id, country) 
SELECT film_id,country FROM films_import_step2,
json_array_elements_text(films_import_step2.countries::json) as country;

\echo
\echo Import to table film_links
INSERT INTO film_links (url, notes, film_id)
SELECT url,t.notes,film_id FROM films_import_step2,
json_to_recordset(films_import_step2.links::json) AS t(url text, notes text);

\echo 
\echo Search for broken credit-relations (credit_id is NULL or credit_id not in table credits)
SELECT films_import_step2.title,film_id,id,t.title,role,"as",name_variation FROM films_import_step2,
json_to_recordset(films_import_step2.credits::json) AS t(id int, title text, role text, "as" text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT credit_id FROM credits WHERE credit_id=id));

\echo
\echo Import to table film_credits
INSERT INTO film_credits (film_id, credit_id, title, role, act_as,name_variation)
SELECT film_id,id,t.title,role,"as",name_variation FROM films_import_step2,
json_to_recordset(films_import_step2.credits::json) AS t(id int, title text, role text, "as" text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT credit_id FROM credits));

-- remove temporary table
\echo
\echo Clean up
DROP TABLE films_import_step2;
\echo
\echo Finished import films
\echo

