\echo 
\echo Start import makers
\echo -------------------

-- Disabled because database is dropped
--DROP TABLE public.maker_links;
--DROP TABLE public.maker_events;
--DROP TABLE public.makers;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE maker_import_step1;
DROP TABLE maker_import_step2;

\echo
\echo Create tables
CREATE TABLE public.makers (
    maker_id integer,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default",
    profile text COLLATE pg_catalog."default",
    CONSTRAINT makers_pkey PRIMARY KEY (maker_id),
    CONSTRAINT makers_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.maker_events (
    maker_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    event text COLLATE pg_catalog."default",
    place text COLLATE pg_catalog."default",
    maker_id integer NOT NULL,
    CONSTRAINT maker_events_pkey PRIMARY KEY (maker_event_id),
    CONSTRAINT maker_events_maker_id_fkey FOREIGN KEY (maker_id)
        REFERENCES public.makers (maker_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.maker_links (
    maker_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    maker_id integer NOT NULL,
    CONSTRAINT maker_links_pkey PRIMARY KEY (maker_link_id),
    CONSTRAINT maker_links_maker_id_fkey FOREIGN KEY (maker_id)
        REFERENCES public.makers (maker_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to gearogs
ALTER TABLE public.makers OWNER to gearogs;
ALTER TABLE public.maker_events OWNER to gearogs;
ALTER TABLE public.maker_links OWNER to gearogs;

\echo
\echo Extract data from gearogs-makers.json
CREATE TEMP TABLE maker_import_step1(maker json);

\copy maker_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./gearogs-makers.json'

CREATE TEMP TABLE maker_import_step2 (
	maker_id integer,
	title text,
	profile text,
	slug text,
	events json,
	links json
);

-- Extract single rows
INSERT INTO maker_import_step2 (maker_id, title, profile, slug, events, links)
SELECT id,title,profile,slug,events,external_links FROM maker_import_step1,
json_to_recordset(maker_import_step1.maker::json) AS t(id int, title text, profile text, slug text, events json, external_links json);

DROP TABLE maker_import_step1;

\echo
\echo Import to table makers
INSERT INTO makers (maker_id, title, slug, profile)
SELECT maker_id, title, slug, profile FROM maker_import_step2;

\echo
\echo Import to table maker_events
INSERT INTO maker_events (date, place, event, maker_id)
SELECT date,place,event,maker_id FROM maker_import_step2,
json_to_recordset(maker_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table maker_links
INSERT INTO maker_links (url, notes, maker_id)
SELECT url,notes,maker_id FROM maker_import_step2,
json_to_recordset(maker_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE maker_import_step2;
\echo
\echo Finished import makers
\echo

