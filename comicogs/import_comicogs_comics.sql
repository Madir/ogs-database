\echo 
\echo Start import credits
\echo --------------------

-- Disabled because database is dropped
--DROP TABLE public.comic_collects;
--DROP TABLE public.comic_series;
--DROP TABLE public.comic_credits;
--DROP TABLE public.comic_publishers;
--DROP TABLE public.comic_codes;
--DROP TABLE public.comic_characters;
--DROP TABLE public.comic_links;
--DROP TABLE public.comics;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE comic_import_step1;
DROP TABLE comic_import_step2;

\echo
\echo Create tables
CREATE TABLE public.comics (
    comic_id integer NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    format text COLLATE pg_catalog."default",
    volume text COLLATE pg_catalog."default",
    issue text COLLATE pg_catalog."default",
    collection_volume integer,
    date text COLLATE pg_catalog."default",
    location_of_publication text COLLATE pg_catalog."default",
    genre text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    page_count text COLLATE pg_catalog."default",
    language text COLLATE pg_catalog."default",
    location text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    CONSTRAINT comics_pkey PRIMARY KEY (comic_id)
) TABLESPACE pg_default;

CREATE TABLE public.comic_links (
    comic_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    comic_id integer NOT NULL,
    CONSTRAINT comic_links_pkey PRIMARY KEY (comic_link_id),
    CONSTRAINT comic_links_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.comic_characters (
    comic_character_id serial NOT NULL,
    comic_id integer NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT comic_characters_pkey PRIMARY KEY (comic_character_id),
--    CONSTRAINT comic_characters_comic_id_character_key UNIQUE (comic_id, name),
    CONSTRAINT comic_characters_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.comic_codes (
    comic_code_id serial NOT NULL,
    comic_id integer NOT NULL,
    code text COLLATE pg_catalog."default" NOT NULL,
    type text COLLATE pg_catalog."default",
    CONSTRAINT comic_codes_pkey PRIMARY KEY (comic_code_id),
    CONSTRAINT comic_codes_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.comic_series (
    comic_series_id serial NOT NULL,
    comic_id integer NOT NULL,
    series_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT comic_series_pkey PRIMARY KEY (comic_series_id),
    CONSTRAINT comic_series_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT comic_series_series_id_fkey FOREIGN KEY (series_id)
        REFERENCES public.series (series_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.comic_publishers (
    comic_publisher_id serial NOT NULL,
    comic_id integer NOT NULL,
    publisher_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT comic_publishers_pkey PRIMARY KEY (comic_publisher_id),
    CONSTRAINT comic_publishers_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT comic_publishers_publisher_id_fkey FOREIGN KEY (publisher_id)
        REFERENCES public.publishers (publisher_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.comic_credits (
    comic_credit_id serial NOT NULL,
    comic_id integer NOT NULL,
    credit_id integer,
    title text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT comic_credits_pkey PRIMARY KEY (comic_credit_id),
    CONSTRAINT comic_credits_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT comic_credits_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.comic_collects (
    comic_collects_id serial NOT NULL,
    comic_id integer NOT NULL,
    collect_comic_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT comic_collects_pkey PRIMARY KEY (comic_collects_id),
    CONSTRAINT comic_collects_comic_id_fkey FOREIGN KEY (comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT comic_collects_collect_comic_id_fkey FOREIGN KEY (collect_comic_id)
        REFERENCES public.comics (comic_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to comicogs
ALTER TABLE public.comics OWNER to comicogs;
ALTER TABLE public.comic_links OWNER to comicogs;
ALTER TABLE public.comic_characters OWNER to comicogs;
ALTER TABLE public.comic_codes OWNER to comicogs;
ALTER TABLE public.comic_series OWNER to comicogs;
ALTER TABLE public.comic_publishers OWNER to comicogs;
ALTER TABLE public.comic_credits OWNER to comicogs;
ALTER TABLE public.comic_collects OWNER to comicogs;


\echo
\echo Extract data from comicogs-comics.json
CREATE TEMP TABLE comic_import_step1(comic json);

\copy comic_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./comicogs-comics.json'

CREATE TEMP TABLE comic_import_step2(
	comic_id int,
    	slug text,
	title text,
        format text,
	volume text,
	issue text,
    	collection_volume int,
    	date text,
    	location_of_publication text,
    	genre text,
    	type text,
    	page_count text,
    	language text,
	location text,
	notes text,
	chars json,
	series json,
	collects json,
	credits json,
	publishers json,
	identifying_codes json,
	links json
);

-- Extract single rows
INSERT INTO comic_import_step2(comic_id, slug, title, format, volume, issue, collection_volume, date, location_of_publication, genre, type, page_count, language, location, notes, chars, series, collects, credits, publishers, identifying_codes, links ) 
SELECT id, slug, title, format, volume, issue, collection_volume, date, location_of_publication, genre, type, page_count, language, location, notes, characters, series, collects, creators, publishers, identifying_codes, external_links FROM comic_import_step1,
json_to_recordset(comic_import_step1.comic::json) AS t(id int, slug text, title text, format text, volume text, issue text, collection_volume int, date text, location_of_publication text, genre text, type text, page_count text, language text, location text, notes text, characters json, series json, collects json, creators json, publishers json, identifying_codes json, external_links json);

DROP TABLE comic_import_step1;

\echo
\echo Import to table comics
INSERT INTO comics (comic_id, slug, title, format, volume, issue, collection_volume, date, location_of_publication, genre, type, page_count, language, location, notes) 
SELECT comic_id, slug, title, format, volume, issue, collection_volume, date, location_of_publication, genre, type, page_count, language, location, notes FROM comic_import_step2;

\echo
\echo Import to table comic_links
INSERT INTO comic_links (url, notes, comic_id)
SELECT url,t.notes,comic_id FROM comic_import_step2,
json_to_recordset(comic_import_step2.links::json) AS t(url text, notes text);

\echo
\echo Import to table comic_characters
INSERT INTO comic_characters (name, comic_id)
SELECT name,comic_id FROM comic_import_step2,
json_to_recordset(comic_import_step2.chars::json) AS t(name text);

\echo
\echo Import to table comic_codes
INSERT INTO comic_codes (code, type, comic_id)
SELECT code,t.type,comic_id FROM comic_import_step2,
json_to_recordset(comic_import_step2.identifying_codes::json) AS t(code text, type text);

\echo 
\echo Search for broken credit-relations (credit_id is NULL or credit_id not in table credits)
SELECT comic_import_step2.title,comic_id,id,t.title,role,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.credits::json) AS t(id int, title text,role text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT credit_id FROM credits WHERE credit_id=id));

\echo
\echo Import to table comic_credits
INSERT INTO comic_credits (comic_id, credit_id, title, role, name_variation)
SELECT comic_id,id,t.title,role,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.credits::json) AS t(id int, title text,role text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT credit_id FROM credits));

\echo 
\echo Search for broken publisher-relations (publisher_id is NULL or publisher_id not in table publishers)
SELECT comic_import_step2.title,comic_id,id,t.title,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.publishers::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT publisher_id FROM publishers WHERE publisher_id=id));

\echo
\echo Import to table comic_publishers
INSERT INTO comic_publishers (comic_id, publisher_id, title, name_variation)
SELECT comic_id,id,t.title,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.publishers::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT publisher_id FROM publishers));

\echo 
\echo Search for broken series-relations (series_id is NULL or series_id not in table series)
SELECT comic_import_step2.title,comic_id,id,t.title,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.series::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT series_id FROM series WHERE series_id=id));

\echo
\echo Import to table comic_series
INSERT INTO comic_series (comic_id, series_id, title, name_variation)
SELECT comic_id,id,t.title,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.series::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT series_id FROM series));

\echo 
\echo Search for broken collects-relations (comic_id is NULL or comic_id not in table ccomics)
SELECT comic_import_step2.title,comic_id,id,t.title,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.collects::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT comic_id FROM comics WHERE comic_id=id));

\echo
\echo Import to table comic_collects
INSERT INTO comic_collects (comic_id, collect_comic_id, title, name_variation)
SELECT comic_id,id,t.title,name_variation FROM comic_import_step2,
json_to_recordset(comic_import_step2.collects::json) AS t(id int, title text, name_variation text)
WHERE (id is NOT NULL) AND (id IN (SELECT comic_id FROM comics));

-- remove temporary table
\echo
\echo Clean up
DROP TABLE comic_import_step2;
\echo
\echo Finished import comics
\echo
