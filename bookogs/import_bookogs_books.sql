\echo
\echo Start import books
\echo ------------------

-- Disabled because database is dropped
--DROP TABLE public.book_works ;
--DROP TABLE public.book_credits;
--DROP TABLE public.book_codes;
--DROP TABLE public.book_dates;
--DROP TABLE public.book_links;
--DROP TABLE public.book_weights;
--DROP TABLE public.book_dimensions;
--DROP TABLE public.book_genres;
--DROP TABLE public.book_languages;
--DROP TABLE public.books;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE book_import_step1;
DROP TABLE book_import_step2;

\echo
\echo Create tables
CREATE TABLE public.books (
    book_id integer NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    format text COLLATE pg_catalog."default",
    page_count text COLLATE pg_catalog."default",
    location text COLLATE pg_catalog."default",
    chapters text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    font text COLLATE pg_catalog."default",
    original_title text COLLATE pg_catalog."default",
    original_language text COLLATE pg_catalog."default",
    CONSTRAINT books_pkey PRIMARY KEY (book_id)
) TABLESPACE pg_default;

CREATE TABLE public.book_languages (
    book_language_id serial,
    book_id integer NOT NULL,
    language text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT book_languages_pkey PRIMARY KEY (book_language_id),
    CONSTRAINT book_languages_book_id_language_key UNIQUE (book_id, language),
    CONSTRAINT book_languages_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_genres (
    book_genres_id serial NOT NULL,
    book_id integer NOT NULL,
    genre text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT book_genres_pkey PRIMARY KEY (book_genres_id),
--    CONSTRAINT book_genres_book_id_language_key UNIQUE (book_id, genre),
    CONSTRAINT book_genres_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_dimensions (
    book_dimension_id serial NOT NULL,
    book_id integer NOT NULL,
    width numeric,
    height numeric,
    depth numeric,
    notes text COLLATE pg_catalog."default",
    CONSTRAINT book_dimensions_pkey PRIMARY KEY (book_dimension_id),
    CONSTRAINT book_dimensions_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_weights (
    book_weight_id serial NOT NULL,
    book_id integer NOT NULL,
    weight numeric,
    notes text COLLATE pg_catalog."default",
    CONSTRAINT book_weights_pkey PRIMARY KEY (book_weight_id),
    CONSTRAINT book_weights_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_links (
    book_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    book_id integer NOT NULL,
    CONSTRAINT book_links_pkey PRIMARY KEY (book_link_id),
    CONSTRAINT book_links_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_dates (
    book_date_id serial NOT NULL,
    book_id integer NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    type text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT book_dates_pkey PRIMARY KEY (book_date_id),
    CONSTRAINT book_dates_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;
    
CREATE TABLE public.book_codes (
    book_code_id serial NOT NULL,
    book_id integer NOT NULL,
    code text COLLATE pg_catalog."default" NOT NULL,
    type text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT book_codes_pkey PRIMARY KEY (book_code_id),
    CONSTRAINT book_codes_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_credits (
    book_credit_id serial,
    book_id integer NOT NULL,
    credit_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT book_credits_pkey PRIMARY KEY (book_credit_id),
    CONSTRAINT book_credits_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT book_credits_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.book_works (
    book_work_id serial,
    book_id integer NOT NULL,
    work_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT book_works_pkey PRIMARY KEY (book_work_id),
    CONSTRAINT book_works_work_id_fkey FOREIGN KEY (work_id)
        REFERENCES public.works (work_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT book_works_book_id_fkey FOREIGN KEY (book_id)
        REFERENCES public.books (book_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to bookogs
ALTER TABLE public.books OWNER to bookogs;
ALTER TABLE public.book_languages OWNER to bookogs;
ALTER TABLE public.book_genres OWNER to bookogs;
ALTER TABLE public.book_dimensions OWNER to bookogs;
ALTER TABLE public.book_weights OWNER to bookogs;
ALTER TABLE public.book_links OWNER to bookogs;
ALTER TABLE public.book_dates OWNER to bookogs;
ALTER TABLE public.book_codes OWNER to bookogs;
ALTER TABLE public.book_credits OWNER to bookogs;
ALTER TABLE public.book_works OWNER to bookogs;    

\echo
\echo Extract data from bookogs-books.json
CREATE TEMP TABLE book_import_step1(book json);

\copy book_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./bookogs-books.json'

CREATE TEMP TABLE book_import_step2(
	book_id int,
	slug text,
	title text,
	format text,
	page_count text,
	location text,
	chapters text,
	notes text,
	font text,
	original_title text,
	original_language text,
	credits json,
	dimensions json,
	languages json,
	dates json,
	genre json,
	identifying_codes json,
	works json,
	weights json,
	links json
	);

-- Extract single rows
INSERT INTO book_import_step2(book_id, slug, title, format, page_count, location, chapters, notes, font, original_title, original_language, credits, dimensions, languages, dates, genre, identifying_codes, works, weights, links) 
SELECT id, slug, title, format, page_count, location, chapters, notes, font, original_title, original_language, credits, dimensions, languages, dates, genre, identifying_codes, works, weights, external_links FROM book_import_step1,
json_to_recordset(book_import_step1.book::json) AS t(id int, slug text, title text, format text, page_count text, location text, chapters text, notes text, font text, original_title text, original_language text, credits json, dimensions json, languages json, dates json, genre json, identifying_codes json, works json, weights json, external_links json);

DROP TABLE book_import_step1;

\echo
\echo Import to table books
INSERT INTO books (book_id, slug, title, format, page_count, location, chapters, notes, font, original_title, original_language) 
SELECT book_id, slug, title, format, page_count, location, chapters, notes, font, original_title, original_language FROM book_import_step2;

\echo
\echo Import to table book_languages
INSERT INTO book_languages (book_id, language) 
SELECT book_id,lang FROM book_import_step2,
json_array_elements_text(book_import_step2.languages::json) as lang;

\echo
\echo Import to table book_genres
INSERT INTO book_genres (book_id, genre)
SELECT book_id,t.genre FROM book_import_step2,
json_to_recordset(book_import_step2.genre::json) AS t(genre text);

\echo
\echo Import to table book_dimensions
INSERT INTO book_dimensions (book_id, width, height, depth, notes)
SELECT book_id,width,height,depth,t.notes FROM book_import_step2,
json_to_recordset(book_import_step2.dimensions::json) AS t(width numeric, height numeric, depth numeric, notes text);

\echo
\echo Import to table book_weights
INSERT INTO book_weights (book_id, weight, notes)
SELECT book_id,weight,t.notes FROM book_import_step2,
json_to_recordset(book_import_step2.weights::json) AS t(weight numeric, notes text);

\echo
\echo Import to table book_links
INSERT INTO book_links (url, notes, book_id)
SELECT url,t.notes,book_id FROM book_import_step2,
json_to_recordset(book_import_step2.links::json) AS t(url text, notes text);

\echo
\echo Import to table book_dates
INSERT INTO book_dates (date, type, book_id)
SELECT date,type,book_id FROM book_import_step2,
json_to_recordset(book_import_step2.dates::json) AS t(date text, type text);

\echo
\echo Import to table book_codes
INSERT INTO book_codes (code, type, book_id)
SELECT code,type,book_id FROM book_import_step2,
json_to_recordset(book_import_step2.identifying_codes::json) AS t(code text, type text);

\echo 
\echo Search for broken credit-relations (credit_id is NULL or credit_id not in table credits)
SELECT book_import_step2.title,book_id,id,t.title,role,name_variation FROM book_import_step2,
json_to_recordset(book_import_step2.credits::json) AS t(id int, title text,role text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT credit_id FROM credits WHERE credit_id=id));

\echo
\echo Import to table book_credits
INSERT INTO book_credits (book_id, credit_id, title, role, name_variation)
SELECT book_id,id,t.title,role,name_variation FROM book_import_step2,
json_to_recordset(book_import_step2.credits::json) AS t(id int, title text,role text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT credit_id FROM credits));

\echo 
\echo Search for broken work-relations (work_id is NULL or work_id not in table works)
SELECT book_import_step2.title,book_id,id,t.title,name_variation FROM book_import_step2,
json_to_recordset(book_import_step2.works::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT work_id FROM works WHERE work_id=id));

\echo
\echo Import to table book_works
INSERT INTO book_works (book_id, work_id, title, name_variation)
SELECT book_id,id,t.title,name_variation FROM book_import_step2,
json_to_recordset(book_import_step2.works::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT work_id FROM works));

-- remove temporary table
\echo
\echo Clean up
DROP TABLE book_import_step2;
\echo
\echo Finished import books
\echo
