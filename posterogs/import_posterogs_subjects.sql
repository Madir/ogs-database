\echo 
\echo Start import subjects
\echo ---------------------

-- Disabled because database is dropped
--DROP TABLE public.subject_links;
--DROP TABLE public.subject_events;
--DROP TABLE public.subjects;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE subject_import_step1;
DROP TABLE subject_import_step2;

\echo
\echo Create tables
CREATE TABLE public.subjects (
    subject_id integer,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default",
    profile text COLLATE pg_catalog."default",
    CONSTRAINT subjects_pkey PRIMARY KEY (subject_id),
    CONSTRAINT subjects_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.subject_events (
    subject_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    event text COLLATE pg_catalog."default",
    place text COLLATE pg_catalog."default",
    subject_id integer NOT NULL,
    CONSTRAINT subject_events_pkey PRIMARY KEY (subject_event_id),
    CONSTRAINT subject_events_subject_id_fkey FOREIGN KEY (subject_id)
        REFERENCES public.subjects (subject_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.subject_links (
    subject_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    subject_id integer NOT NULL,
    CONSTRAINT subject_links_pkey PRIMARY KEY (subject_link_id),
    CONSTRAINT subject_links_subject_id_fkey FOREIGN KEY (subject_id)
        REFERENCES public.subjects (subject_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to posterogs
ALTER TABLE public.subjects OWNER to posterogs;
ALTER TABLE public.subject_events OWNER to posterogs;
ALTER TABLE public.subject_links OWNER to posterogs;

\echo
\echo Extract data from posterogs-subjects.json
CREATE TEMP TABLE subject_import_step1(subject json);

\copy subject_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./posterogs-subjects.json'

CREATE TEMP TABLE subject_import_step2 (
	subject_id integer,
	title text,
	profile text,
	slug text,
	events json,
	links json
);

-- Extract single rows
INSERT INTO subject_import_step2 (subject_id, title, profile, slug, events, links)
SELECT id,title,profile,slug,events,external_links FROM subject_import_step1,
json_to_recordset(subject_import_step1.subject::json) AS t(id int, title text, profile text, slug text, events json, external_links json);

DROP TABLE subject_import_step1;

\echo
\echo Import to table subjects
INSERT INTO subjects (subject_id, title, slug, profile)
SELECT subject_id, title, slug, profile FROM subject_import_step2;

\echo
\echo Import to table subject_events
INSERT INTO subject_events (date, place, event, subject_id)
SELECT date,place,event,subject_id FROM subject_import_step2,
json_to_recordset(subject_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table subject_links
INSERT INTO subject_links (url, notes, subject_id)
SELECT url,notes,subject_id FROM subject_import_step2,
json_to_recordset(subject_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE subject_import_step2;
\echo
\echo Finished import subjects
\echo

