\echo 
\echo Start import posters
\echo ---------------------

-- Disabled because database is dropped
--DROP TABLE public.poster_master;
--DROP TABLE public.poster_setlist;
--DROP TABLE public.poster_credits;
--DROP TABLE public.poster_venues;
--DROP TABLE public.poster_subjects;
--DROP TABLE public.poster_links;
--DROP TABLE public.poster_dates;
--DROP TABLE public.poster_countries;
--DROP TABLE public.posters;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE posters_import_step1;
DROP TABLE posters_import_step2;

\echo
\echo Create tables
CREATE TABLE public.posters (
    poster_id integer NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    subtitle text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    width numeric,
    height numeric,
    limited_run_of numeric,
    print_technique text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    price_amount numeric,
    price_currency text COLLATE pg_catalog."default",    
    CONSTRAINT posters_pkey PRIMARY KEY (poster_id)
) TABLESPACE pg_default;

CREATE TABLE public.poster_countries (
    poster_country_id serial NOT NULL,
    country text COLLATE pg_catalog."default",
    poster_id integer NOT NULL,
    CONSTRAINT poster_countries_pkey PRIMARY KEY (poster_country_id),
    CONSTRAINT poster_countries_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_dates (
    poster_date_id serial NOT NULL,
    date text COLLATE pg_catalog."default",
    note text COLLATE pg_catalog."default",
    poster_id integer NOT NULL,
    CONSTRAINT poster_dates_pkey PRIMARY KEY (poster_date_id),
    CONSTRAINT poster_dates_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_links (
    poster_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    poster_id integer NOT NULL,
    CONSTRAINT poster_links_pkey PRIMARY KEY (poster_link_id),
    CONSTRAINT poster_links_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_subjects (
    poster_subject_id serial NOT NULL,
    poster_id integer NOT NULL,
    subject_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT poster_subjects_pkey PRIMARY KEY (poster_subject_id),
    CONSTRAINT poster_subjects_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT poster_subjects_subject_id_fkey FOREIGN KEY (subject_id)
        REFERENCES public.subjects (subject_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_venues (
    poster_venue_id serial NOT NULL,
    poster_id integer NOT NULL,
    venue_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT poster_venues_pkey PRIMARY KEY (poster_venue_id),
    CONSTRAINT poster_venues_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT poster_venues_venue_id_fkey FOREIGN KEY (venue_id)
        REFERENCES public.venues (venue_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_credits (
    poster_credit_id serial NOT NULL,
    poster_id integer NOT NULL,
    credit_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT poster_credits_pkey PRIMARY KEY (poster_credit_id),
    CONSTRAINT poster_credits_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT poster_credits_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_setlist (
    poster_setlist_id serial NOT NULL,
    poster_id integer NOT NULL,
    subject_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
-- No song title in current export, maybe defined later  
--    song_title text COLLATE pg_catalog."default",
    url text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT poster_setlist_pkey PRIMARY KEY (poster_setlist_id),
    CONSTRAINT poster_setlist_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
   CONSTRAINT poster_setlist_subject_id_fkey FOREIGN KEY (subject_id)
        REFERENCES public.subjects (subject_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.poster_master(
    poster_master_id serial NOT NULL,
    poster_id integer NOT NULL, 
    master_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT poster_master_pkey PRIMARY KEY (poster_master_id),
    CONSTRAINT poster_master_poster_id_fkey FOREIGN KEY (poster_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
   CONSTRAINT poster_master_master_id_fkey FOREIGN KEY (master_id)
        REFERENCES public.posters (poster_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to posterogs
ALTER TABLE public.posters OWNER to posterogs;
ALTER TABLE public.poster_countries OWNER to posterogs;
ALTER TABLE public.poster_dates OWNER to posterogs;
ALTER TABLE public.poster_links OWNER to posterogs;
ALTER TABLE public.poster_subjects OWNER to posterogs;
ALTER TABLE public.poster_venues OWNER to posterogs;
ALTER TABLE public.poster_credits OWNER to posterogs;
ALTER TABLE public.poster_setlist OWNER to posterogs;
ALTER TABLE public.poster_master OWNER to posterogs;

\echo
\echo Extract data from posterogs-posters.json
CREATE TEMP TABLE posters_import_step1(posters json);

\copy posters_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./posterogs-posters.json'

CREATE TEMP TABLE posters_import_step2(
	poster_id int,
    	slug text,
	title text,
        subtitle text,
	type text,
	width numeric,
    	height numeric,
	limited_run_of numeric,
    	print_technique text,
	notes text,
	price json,
	countries json,
	dates json,
	links json,
	subjects json,		
	venues json,
	credits json,
	setlist json,
	master json
);

-- Extract single rows
INSERT INTO posters_import_step2(poster_id, slug, title, subtitle, type, width, height, limited_run_of, print_technique, notes, price, countries, dates, links, subjects, venues, credits, setlist, master)
SELECT id, slug, title, subtitle, type, width, height, edition_of, technique, notes, original_price, countries, dates, external_links, subjects, venues, credits, setlist, master FROM posters_import_step1,
json_to_recordset(posters_import_step1.posters::json) AS t(id int, slug text, title text, subtitle text, type text, width numeric, height numeric, edition_of numeric, technique text, notes text, original_price json, countries json, dates json, external_links json, subjects json, venues json, credits json, setlist json, master json);

DROP TABLE posters_import_step1;

\echo
\echo Import to table posters
INSERT INTO posters (poster_id, slug, title, subtitle, type, width, height, limited_run_of, print_technique, notes, price_amount, price_currency) 
SELECT poster_id, slug, title, subtitle, type, width, height, limited_run_of, print_technique, notes, amount, currency FROM posters_import_step2,
json_to_record(posters_import_step2.price::json) AS t(amount numeric, currency text);

\echo
\echo Import to table poster_countries
INSERT INTO poster_countries (country, poster_id)
SELECT country,poster_id FROM posters_import_step2,
json_array_elements_text(posters_import_step2.countries::json) as country;

\echo
\echo Import to table poster_dates
INSERT INTO poster_dates (date, note, poster_id)
SELECT date,note,poster_id FROM posters_import_step2,
json_to_recordset(posters_import_step2.dates::json) AS t(date text, note text);

\echo
\echo Import to table poster_links
INSERT INTO poster_links(url, notes, poster_id)
SELECT url,t.notes,poster_id FROM posters_import_step2,
json_to_recordset(posters_import_step2.links::json) AS t(url text, notes text);

\echo 
\echo Search for broken subject-relations (subject_id is NULL or subject_id not in table subjects)
SELECT posters_import_step2.title,poster_id,id,t.title,role,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.subjects::json) AS t(id int, title text, role text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT subject_id FROM subjects WHERE subject_id=id));

\echo
\echo Import to table posters_subjects
INSERT INTO poster_subjects (poster_id, subject_id, title, role, name_variation)
SELECT poster_id,id,t.title,role,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.subjects::json) AS t(id int, title text, role text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT subject_id FROM subjects WHERE subject_id=id));

\echo 
\echo Search for broken venue-relations (venue_id is NULL or venue_id not in table venues)
SELECT posters_import_step2.title,poster_id,id,t.title,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.venues::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT venue_id FROM venues WHERE venue_id=id));

\echo
\echo Import to table posters_venues
INSERT INTO poster_venues (poster_id, venue_id, title, name_variation)
SELECT poster_id,id,t.title,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.venues::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT venue_id FROM venues WHERE venue_id=id));

\echo 
\echo Search for broken credit-relations (credit_id is NULL or credit_id not in table credits)
SELECT posters_import_step2.title,poster_id,id,t.title,role,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.credits::json) AS t(id int, title text, role text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT credit_id FROM credits WHERE credit_id=id));

\echo
\echo Import to table posters_credits
INSERT INTO poster_credits (poster_id, credit_id, title, role, name_variation)
SELECT poster_id,id,t.title,role,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.credits::json) AS t(id int, title text, role text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT credit_id FROM credits WHERE credit_id=id));

\echo 
\echo Search for broken setlist-relations (subject_id is NULL or subject_id not in table subjects)
SELECT posters_import_step2.title,poster_id,id,t.title,url,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.setlist::json) AS t(id int, title text, url text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT subject_id FROM subjects WHERE subject_id=id));

\echo
\echo Import to table poster_setlist
INSERT INTO poster_setlist (poster_id, subject_id, title, url, name_variation)
SELECT poster_id,id,t.title,url,name_variation FROM posters_import_step2,
json_to_recordset(posters_import_step2.setlist::json) AS t(id int, title text, url text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT subject_id FROM subjects WHERE subject_id=id));

\echo 
\echo Search for broken master-relations (master_id not in table posters)
SELECT posters_import_step2.title,poster_id,id,t.title,name_variation FROM posters_import_step2,
json_to_record(posters_import_step2.master::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id NOT IN (SELECT poster_id FROM posters WHERE poster_id=id));

\echo
\echo Import to table poster_master
INSERT INTO poster_master (poster_id, master_id, title, name_variation)
SELECT poster_id,id,t.title,name_variation FROM posters_import_step2,
json_to_record(posters_import_step2.master::json) AS t(id int, title text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT poster_id FROM posters WHERE poster_id=id));

-- remove temporary table
\echo
\echo Clean up
DROP TABLE posters_import_step2;
\echo
\echo Finished import posters
\echo
