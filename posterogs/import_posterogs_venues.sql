\echo 
\echo Start import venues
\echo -------------------

-- Disabled because database is dropped
--DROP TABLE public.venue_links;
--DROP TABLE public.venue_events;
--DROP TABLE public.venues;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE venue_import_step1;
DROP TABLE venue_import_step2;

\echo
\echo Create tables
CREATE TABLE public.venues (
    venue_id integer,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default",
    address text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    CONSTRAINT venues_pkey PRIMARY KEY (venue_id),
    CONSTRAINT venues_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.venue_events (
    venue_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    event text COLLATE pg_catalog."default",
    place text COLLATE pg_catalog."default",
    venue_id integer NOT NULL,
    CONSTRAINT venue_events_pkey PRIMARY KEY (venue_event_id),
    CONSTRAINT venue_events_venue_id_fkey FOREIGN KEY (venue_id)
        REFERENCES public.venues (venue_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.venue_links (
    venue_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    venue_id integer NOT NULL,
    CONSTRAINT venue_links_pkey PRIMARY KEY (venue_link_id),
    CONSTRAINT venue_links_venue_id_fkey FOREIGN KEY (venue_id)
        REFERENCES public.venues (venue_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to posterogs
ALTER TABLE public.venues OWNER to posterogs;
ALTER TABLE public.venue_events OWNER to posterogs;
ALTER TABLE public.venue_links OWNER to posterogs;

\echo
\echo Extract data from posterogs-venues.json
CREATE TEMP TABLE venue_import_step1(venue json);

\copy venue_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./posterogs-venues.json'

CREATE TEMP TABLE venue_import_step2 (
	venue_id integer,
	title text,
	description text,
	slug text,
	address json,
	events json,
	links json
);

-- Extract single rows
INSERT INTO venue_import_step2 (venue_id, title, description, address, slug, events, links)
SELECT id,title,description,address,slug,events,external_links FROM venue_import_step1,
json_to_recordset(venue_import_step1.venue::json) AS t(id int, title text, description text, slug text, address json, events json, external_links json);

DROP TABLE venue_import_step1;

\echo
\echo Import to table venues
INSERT INTO venues (venue_id, title, slug, description, address)
SELECT venue_id, title, slug, venue_import_step2.description, t.description FROM venue_import_step2,
json_to_record(venue_import_step2.address::json) AS t(description text);

\echo
\echo Import to table venue_events
INSERT INTO venue_events (date, place, event, venue_id)
SELECT date,place,event,venue_id FROM venue_import_step2,
json_to_recordset(venue_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table venue_links
INSERT INTO venue_links (url, notes, venue_id)
SELECT url,notes,venue_id FROM venue_import_step2,
json_to_recordset(venue_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE venue_import_step2;
\echo
\echo Finished import venues
\echo

