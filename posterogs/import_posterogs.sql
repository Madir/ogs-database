-- WARNING script completely removes the previous database

\echo
\echo STARTED SCRIPT POSTEROGS-IMPORT
\echo ===============================
\echo

\pset pager off

\echo
\echo Remove old database and user
DROP DATABASE posterogs;
DROP OWNED BY posterogs CASCADE;
DROP USER posterogs;

\echo
\echo Create database posterogs and user posterogs
CREATE DATABASE posterogs;
CREATE USER posterogs WITH PASSWORD 'posterogs';
GRANT ALL PRIVILEGES ON DATABASE posterogs TO posterogs;

\connect posterogs

\i ./import_posterogs_credits.sql
\i ./import_posterogs_subjects.sql
\i ./import_posterogs_venues.sql
\i ./import_posterogs_posters.sql

\pset pager on

\echo
\echo FINISHED SCRIPT POSTEROGS-IMPORT
